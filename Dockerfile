FROM iquiw/alpine-emacs as emacs
RUN apk update && apk add git graphviz imagemagick
WORKDIR /root/.emacs.d
COPY elisp/install.el ./
RUN emacs --batch -q -l "install.el"
COPY ./elisp/* ./
RUN git clone https://github.com/fniessen/org-html-themes.git
COPY ./css/* ./
RUN git clone https://gitlab.com/eauc/org-mode.git tmp && \
    cp tmp/lisp/ob-clojure.el elpa/org-plus-contrib*/. && \
    rm elpa/org-plus-contrib*/ob-clojure.elc && \
    rm -rf tmp
